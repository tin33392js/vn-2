// Your code goes here
// (1) find max element
const maxElement = (numbers) => {
	return Math.max(...numbers);
};

// (2) copy array
const copyArray = (array) => {
	return [...array];
};

// (3) add unique id to copied object
const addUniqueId = (oriObject) => {
	const copiedObject = { ...oriObject };
	copiedObject[Symbol()] = null;
	return copiedObject;
};

// (4) regroup object template from nested object
const regroupObject = ({
	name: firstName,
	details: { id, age, university },
}) => {
	const newObj = {
		university,
		user: {
			age,
			firstName,
			id,
		},
	};
	return newObj;
};

// (5) find unique elements
const findUniqueElements = (array) => {
	return [...new Set(array)];
};

// (6) hide phone number
const hideNumber = (phoneNumber) => {
	return phoneNumber
		.toString()
		.slice(-4)
		.padStart(phoneNumber.toString().length, '*');
};

// (7) required all param
const requiredParam = () => {
	throw new Error('Missing property');
};

const add = (a, b = requiredParam(), ...c) => {
	// always require 2 first param
	// if c = [], it still start reduce() at a+b
	return c.reduce((acc, cur) => acc + cur, a + b);
};

// (8) call API with Promise()

const logNameAPIwithThen = () => {
	fetch('https://jsonplaceholder.typicode.com/users')
		.then((response) => response.json())
		.then((arr) => {
			const newArr = [];
			arr.forEach((obj) => newArr.push(obj.name));
			console.log([...newArr].sort());
		});
};

// (9) rewrite, instead Promise() use async/await
const logNameAPIwithAsync = async () => {
	const newArr = [];
	const response = await fetch('https://jsonplaceholder.typicode.com/users');
	const arr = await response.json();
	arr.forEach((obj) => newArr.push(obj.name));
	// (if value is Promise) we have to wait
	// json() is a method return Promise
	console.log([...newArr].sort());
};

// async make f() become asynchronous
// await pause the "asynchronous function" until request complete

// ----------------------- TEST AREA START -----------------------

// // // (1) find max element
// const array1 = [1, 2, 3, 4, 56, 7, 8, 76, 5, 241, 5, 356, 567, 2];
// console.log(maxElement(array1) === 567);
// // // (2) copy array
// const array2 = [1, 2, 3];
// const copiedArray = copyArray(array2);
// console.log(array2.join() === copiedArray.join());
// console.log(!(array2 === copiedArray));
// console.log(addUniqueId({ name: 123 }));
// // // (3) add unique id to copied object
// // // (4) regroup object template from nested object
// const oldObj = {
// 	name: 'Someone',
// 	details: { id: 1, age: 11, university: 'UNI' },
// };
// console.log(regroupObject(oldObj));
// console.log(regroupObject(oldObj).university === 'UNI');
// console.log(regroupObject(oldObj).user.age === 11);
// console.log(regroupObject(oldObj).user.firstName === 'Someone');
// console.log(regroupObject(oldObj).user.id === 1);
// // // (5) find unique elements
// const array5 = [1, 1, 23, 3, 4, 5, 6, 5, 4, 23, 2, 1, 1, 1, 1, 1];

// // // (6) hide phone number
// console.log(findUniqueElements(array5).join() === '1,23,3,4,5,6,2');

// const phoneNumber = '0123456789';
// console.log(hideNumber(phoneNumber) === '******6789');

// // // (7) required all param
// console.log(add(1, 3));
// console.log(add(1, 3, 123123, 12321, 9999999));
// console.log(add(0, 1));
// // console.log(add(1));

// // // (8) call API with Promise()
// logNameAPIwithThen();
// // // (9) rewrite, instead Promise() use async/await
// logNameAPIwithAsync();

// ----------------------- TEST AREA END -----------------------

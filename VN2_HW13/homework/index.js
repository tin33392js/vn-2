// Your code goes here
'use strict';

// ---------------------------------- [1-5] ----------------------------------
const getAge = (dateObj) => {
	const dateToday = new Date();
	let age = dateToday.getFullYear() - dateObj.getFullYear();

	// birth same year ==> age = 0
	if (age === 0) {
		return age;
	}
	// FOUNDATION: today > birthday
	// month < b.month => -1
	// month === b.month => check date
	// month > b.month => igore
	if (dateToday.getMonth() < dateObj.getMonth()) {
		// was wrong when use '>'
		// today: 25/3  | birth: 25/2 --> descrease age (-1) is WRONG
		// just focus test case same month
		age--;
	} else if (
		dateToday.getDate() < dateObj.getDate() &&
		dateToday.getMonth() === dateObj.getMonth()
	) {
		// ignore if today's month smaller
		age--;
	}

	return age;
};

const getWeekDay = (dateOrTime) => {
	const weekDateArr = [
		'Sunday',
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
	];
	return weekDateArr[new Date(dateOrTime).getDay()];
};

const getAmountDaysToNewYear = () => {
	// create a 1st Jan of next Year
	const nextYear = new Date(new Date().getFullYear() + 1, 0, 1);
	//  new Date() only can bigger (in times) --> diffDate smaller
	// --> calculation is <= dates (use .ceil() to reach it)
	const diffDates = (nextYear - new Date()) / 1000 / 60 / 60 / 24;
	// console.log([nextYear, diffDates]);
	return Math.ceil(diffDates);
};

const getProgrammersDay = (year) => {
	return new Date(year, 1, 29).getDate() === 29
		? `12 Sep, ${year} (${getWeekDay(new Date(year, 8, 12))})`
		: `13 Sep, ${year} (${getWeekDay(new Date(year, 8, 13))})`;
};

const howFarIs = (untilDay) => {
	// assume always has 7 seven days a week
	// try indexing each weekday
	// arr: [Mon,Tue,Wed,Thu,Fri,Sat,Sun]
	//      [0,   1,  2,  3,  4,  5,  6]
	const weekArr = [
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
		'Sunday',
	];
	const [a, b] = [
		weekArr.indexOf(getWeekDay(new Date())),
		weekArr.indexOf(
			untilDay[0].toUpperCase() + untilDay.toLowerCase().slice(1)
		),
	];
	if (b > a) {
		// easiest case
		return `It's ${b - a} day(s) left till ${weekArr[b]}.`;
	} else if (b === a) {
		// today
		return `Hey, today is ${weekArr[b]} =)`;
	} else if (b < a) {
		// when it come back to the start of arr
		// (Sun,Mon), (Thu,Wed)
		// count the space from [a] to the [end of week]: (weekArr.length-1) - a
		// count the space from [start of week] to [b]: b - 0
		//  +1 for Sun...Mon
		return `It's ${weekArr.length - 1 - a + (1 + b)} day(s) left till ${
			weekArr[b]
		}.`;
	}
};

// ---------------------------------- [6-12] ----------------------------------
const isValidIdentifier = (str) => {
	const regex = /^[a-zA-Z_$][\w$]*$/;
	// not start w# [0-9], but also in [a-z_]
	// followed by: nothing OR [a-zA-Z_$]
	// flag:
	return regex.test(str);
};

const capitalize = (str) => {
	const regex = /^[a-z]|(?<=\s)[a-z]/g;
	return str.replace(regex, (letter) => letter.toUpperCase());
	// matched string will pass to 'letter'
};

const isValidAudioFile = (str) => {
	// 1+ upper/lower case
	// .mp3 | flac | alac | aac

	const regex = /^[a-zA-Z]+\.(mp3|flac|alac|aac)$/g;
	return regex.test(str);
};

const getHexadecimalColors = (str) => {
	const regex = /#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})(?![a-zA-Z0-9])/g;
	return str.match(regex) !== null ? str.match(regex) : [];
	// [] if not found
};

const isValidPassword = (str) => {
	// const regexArr = [/[A-Z]+/g, /[a-z]+/g, /[0-9]+/g, /.{8,}/g];
	const regex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,}$/;
	// (?=.*[A-Z]) to require occur at least 1
	return regex.test(str);
};
const addThousandsSeparators = (numOrStr) => {
	const regex = /[0-9]{1,3}(?=([0-9]{3})+(?![0-9]))/g;
	// [0-9]{1,3} match 1->3 number ([1] 1,000) ([2] 10,000) ([3] 100,000)
	// (?=([0-9]{3})+) followed by a 'group of 3' with '+' (3x1; 3x2; 3x3 ...)
	// (?![0-9]) will deny if the 'group of 3' followed by another digit --> to prevent (4,5, digits)
	return String(numOrStr).replace(regex, (group3) => group3 + ',');
};

const getAllUrlsFromText = (str) => {
	try {
		if (str !== undefined) {
			const regex = /https?:\/\/[-a-zA-Z0-9@:%._\+~#=]+\//g;
			return str.match(regex) !== null ? str.match(regex) : [];
		} else {
			throw '(error)';
		}
	} catch (err) {
		console.warn(err);
	}
};

// -----------TEST
// console.log(`-------TEST 1`);
// const birthday22 = new Date(2000, 2, 29);
// const birthday23 = new Date(2000, 2, 25);
// console.log(getAge(birthday22) === 21); // 20 (assuming today is the 22nd October)
// console.log(getAge(birthday23) === 22); // 19 (assuming today is the 22nd October)

// console.log(`-------TEST 2`);
// console.log(`Today: ${getWeekDay(Date.now())}`); // "Thursday" (if today is the 22nd October)
// console.log(getWeekDay(new Date(2020, 9, 22)) === 'Thursday'); // "Thursday"

// console.log(`-------TEST 3`);
// console.log(
// 	`Days to newYear - compare with Calculator: ${getAmountDaysToNewYear()}`
// );

// console.log(`-------TEST 4`);
// console.log(getProgrammersDay(2020) === '12 Sep, 2020 (Saturday)'); // "12 Sep, 2020 (Saturday)"
// console.log(getProgrammersDay(2019) === '13 Sep, 2019 (Friday)'); // "13 Sep, 2019 (Friday)"

// console.log(`-------TEST 5s`);
// console.log(howFarIs('monDAY'));
// console.log(howFarIs('tuesday'));
// console.log(howFarIs('Wednesday'));
// console.log(howFarIs('Thursday'));
// console.log(howFarIs('Friday'));
// console.log(howFarIs('Saturday'));
// console.log(howFarIs('Sunday'));

// console.log(`-------TEST 6`);
// console.log(isValidIdentifier('myVar!') === false); // false
// console.log(isValidIdentifier('myVar$') === true); // true
// console.log(isValidIdentifier('myVar_1') === true); // true
// console.log(isValidIdentifier('1_myVar') === false); // false

// console.log(`-------TEST 7`);
// const testStr = 'My name is John Smith. I am 27.';
// console.log(capitalize(testStr) === 'My Name Is John Smith. I Am 27.'); // "My Name Is John Smith. I Am 27."

// console.log(`-------TEST 8`);
// console.log(isValidAudioFile('file.mp4') === false); // false
// console.log(isValidAudioFile('my_file.mp3') === false); // false
// console.log(isValidAudioFile('file.mp3') === true); // true

// console.log(`-------TEST 9`);
// const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';
// console.log(
// 	getHexadecimalColors(testString).join() === ['#3f3', '#AA00ef'].join()
// ); // ["#3f3", "#AA00ef"]
// console.log(getHexadecimalColors('red and #0000').join() === [].join()); // [];

// console.log(`-------TEST 10`);
// console.log(isValidPassword('agent007') === false); // false (no uppercase letter)isValidPassword('AGENT007'); // false (no lowercase letter)
// console.log(isValidPassword('AGENT007') === false); // false (no uppercase letter)isValidPassword('AGENT007'); // false (no lowercase letter)
// console.log(isValidPassword('AgentOOO') === false); // false (no numbers)
// console.log(isValidPassword('Age_007') === false); // false (too short)
// console.log(isValidPassword('Agent007') === true); // true

// console.log(`-------TEST 11`);
// console.log(addThousandsSeparators(1234567890) === '1,234,567,890');
// console.log(addThousandsSeparators('1234567890') === '1,234,567,890');
// console.log(addThousandsSeparators(10) === '10');
// console.log(addThousandsSeparators(1000) === '1,000');
// console.log(addThousandsSeparators(10000) === '10,000');
// console.log(addThousandsSeparators(100000) === '100,000');

// console.log(`-------TEST 12`);
// const text1 =
// 	'We use https://translate.google.com/ to translate some words and phrases from https://angular.io/ ';
// const text2 = 'JavaScript is the best language for beginners!';
// console.log(
// 	getAllUrlsFromText(text1).join() ===
// 		['https://translate.google.com/', 'https://angular.io/'].join()
// ); // ["https://translate.google.com/", "https://angular.io/"]
// console.log(getAllUrlsFromText(text2).join() === [].join()); // []

// console.log(getAllUrlsFromText());
// -----------TEST

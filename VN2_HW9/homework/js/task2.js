// Your code goes here
'use strict';

let attempt, maxRange, randomNum, guessNum, prize, isContinue;

let timeWin = 0;
let totalPrize = 0;
let isPlay = confirm(`Do you want to play a game?`);

// ---------------------------------
while (isPlay) {
	// initial
	// isContinue === null (not be triggered)
	// isContinue !== null === false (not continue > reset to totalPrize, timeWin)
	// isContinue !== null === true (continue)
	isContinue = null;
	attempt = 3;
	maxRange = 8 + 4 * timeWin;
	prize = [100, 50, 25].map((v) => v * (timeWin + 1));
	randomNum = Math.floor(Math.random() * (maxRange + 1));

	while (isContinue === null && attempt > 0) {
		console.log(`random num: ${randomNum}`);
		guessNum = Number(
			prompt(
				`Choose a roulette pocket number from 0 to ${maxRange}
Attemps left: ${attempt}
Total prize: ${totalPrize}$
Possible prize on current attempt: ${prize[prize.length - attempt]}$\n\n`
			)
		);
		if (guessNum === randomNum) {
			// WON: continue or not
			totalPrize += prize[prize.length - attempt];
			timeWin++;
			isContinue = confirm(
				`Congratulation, you won! Your prize is: ${totalPrize} $. Do you want to continue?`
			);
		} else {
			attempt--;
		}
	}

	// LOSE/NOT CONTINUE
	if (attempt === 0 || isContinue === false) {
		alert(`Thank you for your participation. Your prize is: ${totalPrize} $`);
		totalPrize = 0;
		timeWin = 0;
		isPlay = confirm(`Do you want to play a game?`);
	}
}

if (!isPlay) {
	alert(`You did not become a billionaire, but can.`);
}

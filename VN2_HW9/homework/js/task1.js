// Your code goes here
'use strict';

let [initAmount, years, percentOfYear] = [
	prompt('Initital amount of money:'),
	prompt('Number of years:'),
	prompt('Percentage of a year:'),
];

for (const [i, v] of [initAmount, years, percentOfYear].entries()) {
	console.log(i, v);
	if (v.length === 0 || !isFinite(v)) {
		// VALIDATE number
		// empty-string | not-number (string,Infinity,NaN) | true,false via prompt() is "true","false" | null via prompt() is "null"
		alert(`Invalid input data`);
		break;
	}

	if (i === [initAmount, years, percentOfYear].length - 1) {
		// VALIDATE condition, once time
		[initAmount, years, percentOfYear] = [
			Number(initAmount),
			Number(years),
			Number(percentOfYear),
		];
		if (
			// calculate in final iteration (pass validation)
			// isInteger detect "string"
			initAmount >= 1000 &&
			years >= 1 &&
			Number.isInteger(years) &&
			percentOfYear <= 100
		) {
			const totalAmount = initAmount * (percentOfYear / 100 + 1) ** years;
			const totalProfit = totalAmount - initAmount;
			alert(
				`Initial amount: ${initAmount}
Number of years: ${years}
Percentage of year: ${percentOfYear}

Total profit: ${totalProfit.toFixed(2)}
Total amount: ${totalAmount.toFixed(2)}`
			);
		} else {
			alert(`Invalid input data`);
		}
	}
}

// Your code goes here
'use strict';
const isEquals = (a, b) => a === b;

const isBigger = (a, b) => a > b;

const storeNames = (...string) => string;

const getDifference = (a, b) => isBigger(a, b) ? a - b : b - a;

const negativeCount = (arrNum) => arrNum.filter((el) => el < 0).length;
// console.log(negativeCount([4, 3, -2, -9]));

const letterCount = (str1, str2) => {
	let i = 0;
	let count = 0;
	if (str2 === '') {
		return count;
	}
	// Gaurd clause: empty string
	// cause bugs--> always return >-1

	while (str1.indexOf(str2, i) > -1) {
		// -1 when can't find
		// will return value at >= [i position]
		// if [i position] > length --> don't search, return -1
		count++;
		i = str1.indexOf(str2, i) + 1;
	}
	return count;
};

const countPoints = (arrScores) => {
	let finalPoint = 0;
	arrScores.forEach((matchScore) => {
		const indexColon = matchScore.indexOf(':');
		const ourScore = Number(matchScore.slice(0, indexColon));
		const enemyScore = Number(matchScore.slice(indexColon + 1));

		if (isBigger(ourScore, enemyScore)) {
			finalPoint += 3;
		} else if (ourScore === enemyScore) {
			finalPoint++;
		}
		// smaller: not change point --> doesn't need else
	});
	return finalPoint;
};

// ----------------------------- TEST START

// console.log(isEquals(3, 3) === true);
// console.log(isBigger(5, -1) === true);
// console.log(
// 	storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy').join() ===
// 		['Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'].join()
// );
// console.log(getDifference(5, 3) === 2);
// console.log(getDifference(5, 8) === 3);
// console.log(negativeCount([4, 3, 2, 9]) === 0);
// console.log(negativeCount([0, -3, 5, 7]) === 1);
// console.log(letterCount('Marry', 'r') === 2);
// console.log(letterCount('Barny', 'y') === 1);
// console.log(letterCount('', 'z') === 0);
// console.log(
// 	countPoints([
// 		'100:90',
// 		'110:98',
// 		'100:100',
// 		'95:46',
// 		'54:90',
// 		'99:44',
// 		'90:90',
// 		'111:100',
// 	]) === 17
// );

// ----------------------------- TEST END

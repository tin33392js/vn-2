const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin');
const FileManagerPlugin = require('filemanager-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: { index: path.resolve(__dirname, 'js', 'index.js') },
  output: {
    filename: 'app.js',
    clean: true,
    assetModuleFilename: 'img/[name][ext]',
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'], //Preset used for env setup
          },
        },
      },
      // {
      // 	test: /\.(jpe?g|png|gif|svg)$/i,
      // 	type: 'asset',
      // },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'index.html'),
    }),
    new MiniCssExtractPlugin({
      filename: 'style/style.css',
    }),
    new ImageminWebpWebpackPlugin({
      // convert to .webp
      config: [
        {
          test: /\.(jpe?g|png)/,
          options: {
            preset: 'photo',
            quality: 75,
          },
        },
      ],
      overrideExtension: true,
      detailedLogs: false,
      silent: false,
      strict: true,
    }),
    new FileManagerPlugin({
      // delege imgages in /dist; only keep .webp
      events: {
        onStart: {
          // copy: [
          // 	{
          // 		source: './img/*.jpg',
          // 		destination: './img/webp/[name].webp',
          //     format:'webp'
          // 	},

          // ],
          delete: ['./dist/img/*.jpg', './dist/img/*.png'],
        },
      },
    }),
  ],
};

// Your code goes here
import '../style/style.scss';
import { botChoice, playerChoice } from './common/choices.js';
import { compare } from './common/compare.js';
import { displayResult } from './common/displayResult.js';
import { init } from './common/init.js';
import { displayScoreBoard } from './common/displayScoreBoard.js';

const btnWraper = document.querySelector('.btn-wraper');
const btnResetEl = document.querySelector('.btn-reset');
// run init
let stateGame = init();

btnWraper.addEventListener('click', function (e) {
  if (!e.target.classList.contains('btn-game')) {
    return;
  }
  // start game
  // pick choice
  if (stateGame.scorePlayer < 3 && stateGame.scoreBot < 3) {
    // 0. show the score-board
    const scoreBoardEl = document.querySelector('.score-board');
    scoreBoardEl.classList.remove('hidden');
    // 1. (?) i consider .score is more helplful to control then isPlay
    // because click event is also a start btn
    stateGame.choicePlayer = playerChoice.call(e.target);
    stateGame.choiceBot = botChoice();

    stateGame.resultCurrent = compare(
      stateGame.choicePlayer,
      stateGame.choiceBot,
      stateGame
    );
    stateGame.round++;
    // 2.1 display result of current round
    displayResult.call(e.target, stateGame);
    // 2.2 update scoreBoard
    displayScoreBoard(stateGame);
  }
});

btnResetEl.addEventListener('click', (e) => {
  e.preventDefault();
  stateGame = init();
});

// i don't know how to create playGame.js (because it require import)

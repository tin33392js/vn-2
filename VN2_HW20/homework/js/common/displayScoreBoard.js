export const displayScoreBoard = (stateGame) => {
  document.querySelector(
    '.score-board__score'
  ).textContent = `${stateGame.scorePlayer} :
    ${stateGame.scoreBot}`;
};

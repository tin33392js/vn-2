export const init = () => {
  // 1. run function reset
  // 1.1 reset game info
  if (document.querySelectorAll('.result')) {
    document
      .querySelectorAll('.result')
      .forEach((el) => el.classList.remove('result'));
  }
  // 1.2 reset game background's win
  document.querySelector('.pyro').classList.add('hidden');
  // 1.3.1 reset background btn -- endgame WIN
  if (document.querySelector('.btn-game').classList.contains('bk-win')) {
    document
      .querySelectorAll('.btn-game')
      .forEach((el) => el.classList.remove('bk-win'));
  }
  // 1.3.2	 reset background btn -- endgame LOSE
  if (document.querySelector('.btn-game').classList.contains('bk-lose')) {
    document
      .querySelectorAll('.btn-game')
      .forEach((el) => el.classList.remove('bk-lose'));
  }
  // 1.4 hide the score-board
  document.querySelector('.score-board').classList.add('hidden');
  // 2. generate init object
  return {
    round: 0,
    scorePlayer: 0,
    scoreBot: 0,
    // generate those 2 value when start the game
    // choicePlayer : '',
    // choiceBot : '',
  };
};

const btnWraper = document.querySelector('.btn-wraper');

export const botChoice = () => {
  // 1. generate random [0,2]
  const random = Math.floor(Math.random() * btnWraper.childElementCount);
  // 2. read from querySelectorAll
  // *btnChoices.length: to dynamic with numbers of choices
  return btnWraper.children[random].dataset.value;
};

export const playerChoice = function () {
  // this: point to element call it
  return this.dataset.value;
};

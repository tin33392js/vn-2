export const displayResult = function (stateGame) {
  // 0.  reset [start game]
  if (document.querySelector('.result')) {
    document.querySelector('.result').classList.remove('result');
  }
  // 1.  add class
  this.classList.add('result');
  // 2.1 create function to conver String
  const convertString = (str, type = 'title') => {
    // [title, upper]
    if (type === 'title') {
      return str.replace(str[0], str[0].toUpperCase());
    } else if (type === 'upper') {
      return str.toUpperCase();
    }
  };
  // 2.2  add text
  [stateGame.choicePlayer, stateGame.choiceBot, stateGame.resultCurrent] = [
    convertString(stateGame.choicePlayer),
    convertString(stateGame.choiceBot),
    convertString(stateGame.resultCurrent, 'upper'),
  ];
  this.style.setProperty(
    '--resultMessfromJS',
    // eslint-disable-next-line max-len
    `"Round ${stateGame.round}, ${stateGame.choicePlayer} vs. ${stateGame.choiceBot},\\a You're ${stateGame.resultCurrent}"`
  );

  if (stateGame.scorePlayer === 3 || stateGame.scoreBot === 3) {
    // when end game
    const scoreBoardEl = document.querySelector('.score-board');
    scoreBoardEl.classList.add('result');
    // message

    let finalMess = '';

    if (stateGame.scorePlayer > stateGame.scoreBot) {
      finalMess = 'WIN';
      document.querySelector('.pyro').classList.remove('hidden');
      document
        .querySelectorAll('.btn-game')
        .forEach((el) => el.classList.add('bk-win'));
    } else {
      finalMess = 'LOSE';
      document
        .querySelectorAll('.btn-game')
        .forEach((el) => el.classList.add('bk-lose'));
    }

    scoreBoardEl.style.setProperty(
      '--resultMessfromJS',
      `"You're ${finalMess} this time"`
    );
  }
};

const loopCompareArray = ['paper', 'rock', 'scissors', 'paper', 'rock'];

export const compare = (player, bot, stateGame) => {
  // use loopCompareArray --
  // index  (win) index+1: paper > rock
  // index (lose) index+2: paper < scissors
  switch (bot) {
    case loopCompareArray[loopCompareArray.indexOf(player) + 1]:
      stateGame.resultCurrent = 'win';
      stateGame.scorePlayer++;
      break;
    case loopCompareArray[loopCompareArray.indexOf(player) + 2]:
      stateGame.resultCurrent = 'lose';
      stateGame.scoreBot++;
      break;
    default:
      stateGame.resultCurrent = 'draw';
  }

  return stateGame.resultCurrent;
};

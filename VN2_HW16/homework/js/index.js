/* eslint-disable new-cap */
// Your code goes here
'use strict';

// (!) try using disable eslint --> for using first-cap function as normal function, not just constructor
// (!) 2 ways to create read-only property:
// (1) Object.defineProperty w# writable = false --> but can't create dynamic value (base on each time instance call)
// (2) getter <----- we only use this one

// --------------- TASK 1: playing card -------------
// Card() class
class Card {
	constructor(suit, rank) {
		this.suit = suit;
		this.rank = rank;
	}

	toString() {
		// hanlde faceCard
		let rankString = '';
		switch (this.rank) {
			case 1:
				rankString = 'Ace';
				break;
			case 11:
				rankString = 'Jack';
				break;
			case 12:
				rankString = 'Queen';
				break;
			case 13:
				rankString = 'King';
				break;
			default:
				rankString = String(this.rank);
		}
		return `${rankString} of ${this.suit}`;
	}

	get isFaceCard() {
		return this.rank === 1 || this.rank > 10;
	}

	static Compare(cardOne, cardTwo) {
		// use this return to work on Player()
		if (cardOne.rank === cardTwo.rank) {
			return 'equal';
		} else if (cardOne.rank < cardTwo.rank) {
			return 'smaller';
		} else if (cardOne.rank > cardTwo.rank) {
			return 'larger';
		}
	}
}

// Deck() class
class Deck {
	constructor() {
		// temp-variable to run loop to create this.card
		const [suitArr, numberOfRank] = [
			['Spades', 'Clubs', 'Diamonds', 'Hearts'],
			13,
		];

		this.cards = suitArr.flatMap((suit) => {
			const cardsOfSuit = [];
			// each suit, run 13 ranks
			for (let i = 1; i <= numberOfRank; i++) {
				cardsOfSuit.push(new Card(suit, i));
			}
			return cardsOfSuit;
		});
	}

	get count() {
		return this.cards.length;
	}

	shuffle() {
		// 1. inital array, to work inside loop
		const [currentCards, shuffledCards] = [this.cards, []];
		while (currentCards.length > 0) {
			// 2. pick random in range [0,card.length -1]
			const random = Math.floor(Math.random() * currentCards.length);
			// 2.1 remove from currentCards
			// 2.2 add that card to shuffledCards
			shuffledCards.push(currentCards.splice(random, 1)[0]);
		}
		// 3. assign this.card to shullferCard
		this.cards = shuffledCards;
	}

	draw(n) {
		//1. return array of drawed cards
		// start changing from position [-n]
		return this.cards.splice(-n);
	}
}

// Player() class
// name , wins, deck
// method: Play(playerOne,playerTwo)

class Player {
	constructor(name) {
		this.name = name;
		this.deck = new Deck();
		this._wins = 0;
	}

	get wins() {
		return this._wins;
	}

	static Play(playerOne, playerTwo) {
		// 1.1 reset scores each new game
		// 1.2 shuffledCard
		playerOne._wins = playerTwo._wins = 0;
		if (playerOne.deck.count === 0) {
			// 1.3 create new deck if no-card
			playerOne.deck = new Deck();
			playerTwo.deck = new Deck();
		}
		playerOne.deck.shuffle();
		playerTwo.deck.shuffle();
		// 2. draw card until out of cards
		while (playerOne.deck.count > 0 && playerTwo.deck.count > 0) {
			// 3. draw card 1 each player
			// [0] because draw() return array
			const resultCompare = Card.Compare(
				playerOne.deck.draw(1)[0],
				playerTwo.deck.draw(1)[0]
			);
			// 4. increase [wins] based on result compare
			switch (resultCompare) {
				case 'larger':
					playerOne._wins++;
					break;
				case 'smaller':
					playerTwo._wins++;
				default:
					// case 'equal' do nothing
					break;
			}
		}
		// 5. when game end
		if (playerOne.wins > playerTwo.wins) {
			return `${playerOne.name} wins ${playerOne.wins} to ${playerTwo.wins}`;
		} else {
			// include case no-one win into playerTwo win
			return `${playerTwo.name} wins ${playerTwo.wins} to ${playerOne.wins}`;
		}
	}
}
// ----- TEST AREA
// const cardFace = new Card('heart', 1);
// const cardNoFace = new Card('heart', 6);
// console.log(cardFace.isFaceCard === true);
// console.log(cardNoFace.isFaceCard === false);

// const card10ofHeart = new Card('Heart', 10);
// const cardQueenofSpades = new Card('Spades', 12);
// console.log(card10ofHeart.toString() === '10 of Heart');
// console.log(cardQueenofSpades.toString() === 'Queen of Spades');

// const cardCompare1 = new Card('Heart', 10);
// const cardCompare2 = new Card('Heart', 11);
// console.log(Card.Compare(cardCompare1, cardCompare1) === 'equal');
// console.log(Card.Compare(cardCompare1, cardCompare2) === 'smaller');
// console.log(Card.Compare(cardCompare2, cardCompare1) === 'larger');

// const deck1 = new Deck();
// const player1 = new Player('John');
// const player2 = new Player('Smith');
// console.log(Player.Play(player1, player2));
// --------------- TASK 2: employees and managers -------------
// Employee()
class Employee {
	constructor(dataObj) {
		for (let property in dataObj) {
			// 1. loop through input -- to create property of instance
			if (dataObj[property]) {
				this[property] = dataObj[property];
			}
		}
		// 2. add to EMPLOYEES array each time we generate instance
		Employee.EMPLOYEES.push(this);
	}

	get fullName() {
		return this.firstName + ' ' + this.lastName;
	}

	get age() {
		// copy from HW_13
		const dateToday = new Date();
		let age = dateToday.getFullYear() - this.birthday.getFullYear();
		if (age === 0) {
			return age;
		}
		if (dateToday.getMonth() < this.birthday.getMonth()) {
			age--;
		} else if (
			dateToday.getDate() < this.birthday.getDate() &&
			dateToday.getMonth() === this.birthday.getMonth()
		) {
			age--;
		}
		return age;
	}

	quit() {
		// remove from EMPLOYEES
		const index = Employee.EMPLOYEES.find((el) => el.id === this.id);
		Employee.EMPLOYEES.splice(index, 1);
	}

	retire() {
		console.log('It was such a pleasure to work with you!');
		this.quit();
	}
	getFired() {
		console.log('Not a big deal!');
		this.quit();
	}

	changeDepartment(newDepartment) {
		// (?) why change here, also update to Employee.EMPLOYEES
		this.department = newDepartment;
	}
	changePosition(newPosition) {
		this.position = newPosition;
	}
	changeSalary(newSalary) {
		this.salary = newSalary;
	}

	getPromoted(benefits) {
		for (let property in benefits) {
			if (property === 'department') {
				this.changeDepartment(benefits[property]);
			} else if (property === 'position') {
				this.changePosition(benefits[property]);
			} else if (property === 'salary') {
				this.changeSalary(benefits[property]);
			}
		}
		console.log('Yoohoo!');
	}

	getDemoted(punishment) {
		for (let property in punishment) {
			if (property === 'department') {
				this.changeDepartment(punishment[property]);
			} else if (property === 'position') {
				this.changePosition(punishment[property]);
			} else if (property === 'salary') {
				this.changeSalary(punishment[property]);
			}
		}
		console.log('Damn!');
	}
}

Object.defineProperty(Employee, 'EMPLOYEES', {
	// create outside, because 'static property'
	// not allowed by eslint inside class block-code
	value: [],
});

// Manager()

class Manager extends Employee {
	constructor(dataObj) {
		super(dataObj);
		this.position = 'manager';
	}
	get managedEmployees() {
		return Employee.EMPLOYEES.filter((em) => {
			// arrow function, take advantage of lexical 'this'
			// Mangager call > call back inside call
			// 1. same department && no 'manager'
			return em.department === this.department && em.position !== 'manager';
		});
	}
}
// BlueCollarWorker()
class BlueCollarWorker extends Employee {
	constructor(dataObj) {
		super(dataObj);
		console.log(`Less useless - eslint`);
	}
}

// HRManager()
class HRManager extends Manager {
	constructor(dataObj) {
		super(dataObj);
		this.department = 'hr';
	}
}

// SalesManager()
class SalesManager extends Manager {
	constructor(dataObj) {
		super(dataObj);
		this.department = 'sales';
	}
}

// function ManagerPro
const ManagerPro = (managerInstance) => {
	// (!) add many to meet all situation
	// (1) invoke immediately, without argument: benefitr or instance
	managerInstance.managedEmployees.forEach((em) =>
		em.getPromoted({
			salary: 999999,
			position: 'happy to invoke',
		})
	);
	// (2) add Promote() -- without argument: instance
	managerInstance.Promote = (benefits) => {
		managerInstance.managedEmployees.forEach((em) => em.getPromoted(benefits));
	};

	// (3) add PromoteOne() -- with
	managerInstance.PromoteOne = (employeeInstance, benefits) => {
		// 1. check if employee whom manager promote, is in "manageEmployees"
		if (
			managerInstance.managedEmployees.find(
				(em) => em.id === employeeInstance.id
			)
		) {
			// 2.check if employee whom manager promote, is in "manageEmployees"
			employeeInstance.getPromoted(benefits);
		}
	};
};

// ------------------ TEST AREA

// const employee1 = new Employee({
// 	id: 1,
// 	firstName: 'John',
// 	lastName: 'Smith',
// 	birthday: new Date(1999, 3, 3),
// 	salary: 200,
// 	position: 'staff',
// 	department: 'sales',
// });
// const employee2 = new Employee({
// 	id: 2,
// 	firstName: 'Brand',
// 	lastName: 'Scope',
// 	birthday: new Date(1999, 3, 6),
// 	salary: 250,
// 	position: 'boss',
// 	department: 'construct',
// });

// const manager1 = new Manager({
// 	id: 3,
// 	firstName: 'Khoa',
// 	lastName: 'Sizz',
// 	birthday: new Date(1980, 3, 6),
// 	salary: 2500,
// 	position: 'boss',
// 	department: 'construct',
// });

// const blueCollar1 = new BlueCollarWorker({
// 	id: 4,
// 	firstName: 'Kathy',
// 	lastName: 'Simon',
// 	birthday: new Date(1999, 3, 3),
// 	salary: 200,
// 	position: 'newbie',
// 	department: 'sales',
// });
// const manager_hr_1 = new HRManager({
// 	id: 5,
// 	firstName: 'Luoly',
// 	lastName: 'Far',
// 	birthday: new Date(1999, 3, 3),
// 	salary: 200,
// 	position: 'staff',
// 	department: 'hr',
// });
// const manager_sale_1 = new SalesManager({
// 	id: 6,
// 	firstName: 'Santos',
// 	lastName: 'Ares',
// 	birthday: new Date(1999, 3, 3),
// 	salary: 200,
// 	position: 'staff',
// 	department: 'sales',
// });

// employee1.getDemoted({
// 	salary: 10000,
// 	department: 'hr',
// });
// console.log(Employee.EMPLOYEES);

// console.log([employee1, employee2, manager1]);
// console.log(Employee.EMPLOYEES);
// employee1.retire();
// employee2.quit();
// manager1.getFired();

// console.log(Employee.EMPLOYEES);
// console.log([blueCollar1, manager_hr_1, manager_sale_1]);

function reverseNumber(num) {
	let revStr = '';
	let result = '';
	const numString = String(num);

	for (let i = numString.length - 1; i >= 0; i--) {
		revStr = revStr + numString[i];
	}

	if (num < 0) {
		for (let i = 0; i < revStr.length - 1; i++) {
			// exclude '-' at final position
			result += revStr[i];
		}
		result = '-' + result;
	} else {
		result = revStr;
	}
	return Number(result);
}

function forEach(arr, func) {
	for (let i = 0; i < arr.length; i++) {
		func(arr[i]);
	}
}

function map(arr, func) {
	// 1. I stuck w# idea. How to put a return line to forEach when it run loop
	// 2. I stuck with argument of forEach(arr,func) will inherit from map()
	// 3. Solution: take advantage of "execute f() in iteration" of forEach + add our own f() -- make idea (1) become true
	const newArr = [];
	forEach(arr, (el) => {
		newArr[newArr.length] = func(el);
	});

	return newArr;
}

function filter(arr, func) {
	const filteredArr = [];
	forEach(arr, (el) => {
		if (func(el)) {
			filteredArr[filteredArr.length] = el;
		}
	});

	return filteredArr;
}

function getAdultAppleLovers(data) {
	const filterdArr = filter(data, (obj) => {
		return obj.age > 18 && obj.favoriteFruit === 'apple';
	});
	return map(filterdArr, (obj) => obj.name);
}

function getKeys(obj) {
	const keyHolder = [];
	for (const key in obj) {
		keyHolder[keyHolder.length] = key;
	}
	return keyHolder;
}

function getValues(obj) {
	const valueHolder = [];
	for (const key in obj) {
		// key now is a string, not property_name
		valueHolder[valueHolder.length] = obj[key];
	}
	return valueHolder;
}

function showFormattedDate(dateObj) {
	const dateStr = String(dateObj);
	// let [d,m,y] = ['','','']
	// const dateArr = String(dateObj).split(' ');
	// const [d, m, y] = [dateArr[2], dateArr[1], dateArr[3]];
	// console.log(String(dateObj));
	const d = dateStr[8] + dateStr[9];
	const m = dateStr[4] + dateStr[5] + dateStr[6];
	let y = '';
	for (let i = 11; i <= 14; i++) {
		if (!(y === '' && dateStr[i] === '0')) {
			// not add when empty + '0' letter
			y += dateStr[i];
		}
	}
	if (y === '') {
		y = '0';
	}

	return `It is ${d} of ${m}, ${y}`;
}

// ----------------- START TEST
// //Test 1
// console.log(`---Test 1`);

// console.log(reverseNumber(12) === 21); //21
// console.log(reverseNumber(2) === 2); //2
// console.log(reverseNumber(-2333) === -3332); //-3332
// console.log(reverseNumber(2333) === 3332); //3332
// console.log(reverseNumber(8888888889999999) === 9999999888888888); //9999999888888888
// console.log(reverseNumber(-9876543) === -3456789); //-3456789
// console.log(reverseNumber(-111122223333) === -333322221111); //-
// console.log(reverseNumber(-31112) === -21113); //-

// // Test 2
// console.log(`---Test 2`);
// forEach([2, 3, 4], (el) => {
// 	console.log(el);
// }); //2 3 4

// console.log('---');
// forEach([1], (el) => {
// 	console.log(el);
// }); //1

// console.log('---');
// forEach([1, 4, 5, 6], (el) => {
// 	console.log(1 + el);
// }); //2 5 6 7

// console.log('---NOT RETURN');
// console.log(
// 	forEach([1, 4, 5, 6], (el) => {
// 		console.log(`dont return ${el}`);
// 	})
// );

// // Test 3
// console.log(`---Test 3`);
// console.log(
// 	map([2, 5, 8], function (el) {
// 		return el + 3;
// 	}).join() === [5, 8, 11].join()
// ); // returns [5, 8, 11]
// console.log(
// 	map([1, 2, 3, 4, 5], function (el) {
// 		return el * 2;
// 	}).join() === [2, 4, 6, 8, 10].join()
// ); // returns [2, 4, 6, 8, 10]
// console.log(
// 	map([undefined, undefined, undefined], function (el) {
// 		return el;
// 	}).join() === [undefined, undefined, undefined].join()
// ); // returns [undefined, undefined, undefined]

// // Test 4
// console.log(`---Test 4`);
// console.log(
// 	filter([2, 5, 1, 3, 8, 6], function (el) {
// 		return el > 3;
// 	}).join() === [5, 8, 6].join()
// ); //returns [5, 8, 6]
// console.log(
// 	filter([1, 4, 6, 7, 8, 10], function (el) {
// 		return el % 2 === 0;
// 	}).join() === [4, 6, 8, 10].join()
// ); //returns [4, 6, 8, 10]

// // Test 5
// console.log(`---Test 5`);

// const test5 = [
// 	{
// 		_id: '5b5e3168c6bf40f2c1235cd6',
// 		index: 0,
// 		age: 39,
// 		eyeColor: 'green',
// 		name: 'Stein',
// 		favoriteFruit: 'apple',
// 	},
// 	{
// 		_id: '5b5e3168e328c0d72e4f27d8',
// 		index: 1,
// 		age: 38,
// 		eyeColor: 'blue',
// 		name: 'Cortez',
// 		favoriteFruit: 'strawberry',
// 	},
// 	{
// 		_id: '5b5e3168cc79132b631c666a',
// 		index: 2,
// 		age: 2,
// 		eyeColor: 'blue',
// 		name: 'Suzette',
// 		favoriteFruit: 'apple',
// 	},
// 	{
// 		_id: '5b5e31682093adcc6cd0dde5',
// 		index: 3,
// 		age: 17,
// 		eyeColor: 'green',
// 		name: 'Weiss',
// 		favoriteFruit: 'banana',
// 	},
// ];

// console.log(getAdultAppleLovers(test5).join() === ['Stein'].join());

// // Test 6
// console.log(`---Test 6`);
// const test6 = { keyOne: 1, keyTwo: 2, keyThree: 3 };
// console.log(
// 	getKeys({ keyOne: 1, keyTwo: 2, keyThree: 3 }).join() ===
// 		['keyOne', 'keyTwo', 'keyThree'].join()
// ); // returns [“keyOne”,“keyTwo”, “keyThree”]

// // Test 7
// console.log(`---Test 7`);
// console.log(
// 	getValues({ keyOne: 1, keyTwo: 2, keyThree: 3 }).join() === [1, 2, 3].join()
// ); // returns [1, 2, 3]

// // Test 8
// console.log(`---Test 8`);

// console.log(
// 	showFormattedDate(new Date('2018-08-27T01:10:00')) === 'It is 27 of Aug, 2018'
// );
// console.log(
// 	showFormattedDate(new Date('1228-12-31T01:10:00')) === 'It is 31 of Dec, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-11-02T01:10:00')) === 'It is 02 of Nov, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-10-02T01:10:00')) === 'It is 02 of Oct, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-09-02T01:10:00')) === 'It is 02 of Sep, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-08-02T01:10:00')) === 'It is 02 of Aug, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-07-02T01:10:00')) === 'It is 02 of Jul, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-06-02T01:10:00')) === 'It is 02 of Jun, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-05-02T01:10:00')) === 'It is 02 of May, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-04-02T01:10:00')) === 'It is 02 of Apr, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-03-02T01:10:00')) === 'It is 02 of Mar, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-02-02T01:10:00')) === 'It is 02 of Feb, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('1228-01-02T01:10:00')) === 'It is 02 of Jan, 1228'
// );
// console.log(
// 	showFormattedDate(new Date('2038-01-27T01:10:00')) === 'It is 27 of Jan, 2038'
// );
// console.log(
// 	showFormattedDate(new Date('2018-05-27T01:10:00')) === 'It is 27 of May, 2018'
// );
// console.log(
// 	showFormattedDate(new Date('0200-05-27T01:10:00')) === 'It is 27 of May, 200'
// );
// console.log(
// 	showFormattedDate(new Date('0918-05-27T01:10:00')) === 'It is 27 of May, 918'
// );
// console.log(
// 	showFormattedDate(new Date('0020-05-27T01:10:00')) === 'It is 27 of May, 20'
// );
// console.log(
// 	showFormattedDate(new Date('0000-05-27T01:10:00')) === 'It is 27 of May, 0'
// );

// -------------------- END TEST

'use strict';
//---------------------------selector
const root = document.getElementById('root');
const displayTweetPageEl = document.querySelector('#tweetItems');
const modifyTweetPageEl = document.querySelector('#modifyItem');
const alertMessageHolderEl = document.querySelector('#alertMessage');
const alertMessageEl = document.querySelector('#alertMessageText');

const BtnHolderFormTweetEL = document.querySelector('.formButtons');
const h1ListTweetEl = document.querySelector('#tweetItems h1');
const ulListTweetEl = document.querySelector('#tweetItems ul');
const BtnHolderListTweetEl = document.querySelector(
	'#tweetItems #navigationButtons'
);
const h1ModifyTweetEl = document.querySelector('#modifyItem h1');
const textAreaModifyTweetEl = document.querySelector(
	'#modifyItem #modifyItemInput'
);

const link = {
	mainPage: '',
	addTweet: '/add',
	likedPage: '/liked',
	// editTweet: '/edit/:item_id'
};

const Tweet = function (text) {
	this.text = text;
	this.like = false;
};

const addBtnHTML = `<button class="addTweet">Add tweet</button>`;
const gotoLiketnHTML = `<button class="goto-like-btn">Go to liked</button>`;
const backBtnHTML = `<button class="back-btn">back</button>`;

// //---------------------------function
const navigateTo = (linkTarget, elPosition) => {
	if (linkTarget === 'editTweet') {
		// create dynamic based on
		// id = elPosition + 1
		location.hash = `/edit/:item_${elPosition + 1}`;
	} else if (link[linkTarget] !== undefined && link[linkTarget] !== null) {
		location.hash = link[linkTarget];
	}
};

const createTitleBtn = (hash) => {
	// 0.1 add .hidden to modifyTweetPage
	// 0.2 remove .hidden of displayTweetPage
	modifyTweetPageEl.classList.add('hidden');
	displayTweetPageEl.classList.remove('hidden');
	// 1. reset titleBtn
	BtnHolderListTweetEl.textContent = '';
	// (1) run in 'main page'
	// (2) run in 'liked page'
	if (hash === '') {
		// 2.1 change <h1>

		displayTweetPageEl.querySelector('h1').innerText = 'Simple Twitter';
		BtnHolderListTweetEl.insertAdjacentHTML('afterbegin', addBtnHTML);
		document.querySelector('.addTweet').addEventListener('click', () => {
			navigateTo('addTweet');
		});
		// 1. get from local
		const currentLikeLocal = JSON.parse(localStorage.getItem('tweetLikeArr'));
		if (currentLikeLocal.includes(true)) {
			// check if it has 'liked' tweet

			BtnHolderListTweetEl.insertAdjacentHTML('beforeend', gotoLiketnHTML);
			document
				.querySelector('.goto-like-btn')
				.addEventListener('click', () => navigateTo('likedPage'));
		}
	} else if (/liked$/.test(hash)) {
		// 2.2 change <h1>
		displayTweetPageEl.querySelector('h1').innerText = 'Liked Tweets';
		BtnHolderListTweetEl.insertAdjacentHTML('afterbegin', backBtnHTML);
		document
			.querySelector('.back-btn')
			.addEventListener('click', () => navigateTo('mainPage'));
	}
	createTweet();
};

const createTextArea = (elPosition) => {
	// 0.1 add .hidden to displayTweetPage
	// 0.2 remove .hidden of modifyTweetPage
	modifyTweetPageEl.classList.remove('hidden');
	displayTweetPageEl.classList.add('hidden');
	const hash = location.hash;
	if (/\/add$/.test(hash)) {
		modifyTweetPageEl.querySelector('h1').innerText = 'Add tweet';
	} else if (/\/edit\//.test(hash)) {
		modifyTweetPageEl.querySelector('h1').innerText = 'Edit tweet';
	}

	// 1. get from local
	const currentTextLocal = JSON.parse(localStorage.getItem('tweetTextArr'));
	// 2.1 (if) don't pass elPosition -> empty
	// 2.2 (if)  pass elPosition -> search in localStorage
	textAreaModifyTweetEl.value =
		elPosition !== undefined ? currentTextLocal[elPosition] : '';
};

const addTweetLocal = (text) => {
	// 1. get from local
	const currentTextLocal = JSON.parse(localStorage.getItem('tweetTextArr'));
	const currentLikeLocal = JSON.parse(localStorage.getItem('tweetLikeArr'));
	// 2. use .push()
	currentTextLocal.push(text);
	currentLikeLocal.push(false);

	// 3. set to local
	localStorage.setItem('tweetTextArr', JSON.stringify(currentTextLocal));
	localStorage.setItem('tweetLikeArr', JSON.stringify(currentLikeLocal));
};

const removeTweetLocal = (elPosition) => {
	// 1. get from local
	const currentTextLocal = JSON.parse(localStorage.getItem('tweetTextArr'));
	const currentLikeLocal = JSON.parse(localStorage.getItem('tweetLikeArr'));
	// 2. remove element we want
	currentTextLocal.splice(elPosition, 1);
	currentLikeLocal.splice(elPosition, 1);
	// 3. set to local
	localStorage.setItem('tweetTextArr', JSON.stringify(currentTextLocal));
	localStorage.setItem('tweetLikeArr', JSON.stringify(currentLikeLocal));
};

const changeTweetLike = (elPosition) => {
	// 1. get from local
	const currentLikeLocal = JSON.parse(localStorage.getItem('tweetLikeArr'));
	// 2. toggle true/false
	currentLikeLocal[elPosition] = !currentLikeLocal[elPosition];
	// 3. display Alert
	displayAlert(
		`${currentLikeLocal[elPosition] ? 'like' : 'unlike'}`,
		elPosition
	);
	// 4. set to local
	localStorage.setItem('tweetLikeArr', JSON.stringify(currentLikeLocal));
};

const changeTweetText = (elPosition, text) => {
	// 1. get from local
	const currentTextLocal = JSON.parse(localStorage.getItem('tweetTextArr'));
	// 2. change text
	currentTextLocal[elPosition] = text;
	// 3. set to local
	localStorage.setItem('tweetTextArr', JSON.stringify(currentTextLocal));
};

const validateTweetText = (text) => {
	// 1. get from local
	const currentTextLocal = JSON.parse(localStorage.getItem('tweetTextArr'));
	//  2.1 <= 140 char
	//  2.2 not empty
	//  2.3 not duplicate tweet
	if (
		text.length <= 140 &&
		text.length > 0 &&
		!currentTextLocal.includes(text)
	) {
		// 3. return true/false to continue run next code (e.g. changeTweetText, addTweetLocal)
		return true;
	} else {
		// 4. only display error if duplicate
		if (currentTextLocal.includes(text)) {
			displayAlert('error');
		}
		return false;
	}
};

const createTweet = () => {
	// 0. reset current tweet item
	ulListTweetEl.textContent = '';
	// 1. get from local
	const currentTextLocal = JSON.parse(localStorage.getItem('tweetTextArr'));
	const currentLikeLocal = JSON.parse(localStorage.getItem('tweetLikeArr'));
	const hash = location.hash;
	// 2. loop array of local
	// 2.1 add tweetHTML inside <ul>
	if (/liked$/.test(hash)) {
		for (let [i, likeState] of currentLikeLocal.entries()) {
			if (likeState === true) {
				const tweetHTML = `<li class="tweet-item">
		<p class="tweet__text">${currentTextLocal[i]}</p>
		<button class="remove-btn">remove</button>
		<button class="like-btn unlike">${'unlike'}</button>
		</li>`;
				ulListTweetEl.insertAdjacentHTML('beforeend', tweetHTML);
			}
		}
	} else {
		for (let i of currentTextLocal.keys()) {
			const tweetHTML = `<li class="tweet-item">
	<p class="tweet__text">${currentTextLocal[i]}</p>
	<button class="remove-btn">remove</button>
	<button class="like-btn ${currentLikeLocal[i] ? 'unlike' : 'like'}">
	${currentLikeLocal[i] ? 'unlike' : 'like'}</button>
	</li>`;
			ulListTweetEl.insertAdjacentHTML('beforeend', tweetHTML);
		}
	}
};

const updateUI = () => {
	const hash = location.hash;
	createTweet();
	//
	if (hash === '' || /liked$/.test(hash)) {
		createTitleBtn(hash);
	} else if (/\/add$/.test(hash)) {
		createTextArea();
	} else if (/\/edit\//.test(hash)) {
		// 1. pass elPosition by regex
		createTextArea(hash.match(/(?<=item_).+/)[0] - 1);
	}
};

const displayAlert = (typeAlert, elPosition) => {
	// ['like','unlike','error']
	// 1. change alert message

	if (typeAlert === 'like') {
		alertMessageEl.innerText = `Hooray! You liked tweet with id ${
			elPosition + 1
		}!`;
	} else if (typeAlert === 'unlike') {
		alertMessageEl.innerText = `Sorry you no longer like tweet with id ${
			elPosition + 1
		}!`;
	} else if (typeAlert === 'error') {
		alertMessageEl.innerText = `Error! You can't tweet about that`;
	}

	// 2. display alert
	alertMessageHolderEl.classList.remove('hidden');
	// 3. remove alert after 2s
	setTimeout(() => {
		alertMessageEl.innerText = '';
		alertMessageHolderEl.classList.add('hidden');
	}, 2000);
};

// //---------------------------init
if (!localStorage.getItem('tweetTextArr')) {
	(() => {
		localStorage.setItem('tweetTextArr', JSON.stringify([]));
	})();
}
if (!localStorage.getItem('tweetLikeArr')) {
	(() => {
		localStorage.setItem('tweetLikeArr', JSON.stringify([]));
	})();
}

createTweet();
updateUI();
// //---------------------------event handlers
window.addEventListener('hashchange', updateUI);
ulListTweetEl.addEventListener('click', (e) => {
	// 0.1 (if click it-self) return it self <li>
	// 0.2 (if child) return <li>
	// 0.3 (if outsidt) return null
	if (!e.target.closest('.tweet-item')) {
		return;
	}
	const tweetTweet = e.target
		.closest('.tweet-item')
		.querySelector('.tweet__text').textContent;
	// (!) .innerText will trim the string
	// 1. find elPosition, by search indexOf in localStorage
	const currentTextLocal = JSON.parse(localStorage.getItem('tweetTextArr'));
	const elPosition = currentTextLocal.indexOf(tweetTweet);
	if (e.target.classList.contains('remove-btn')) {
		// 2.1 when click Remove
		removeTweetLocal(elPosition);
	} else if (e.target.classList.contains('like-btn')) {
		// 2.2 when click Like
		changeTweetLike(elPosition);
	} else if (e.target.classList.contains('tweet__text')) {
		navigateTo('editTweet', elPosition);
	}
	// 3. update UI
	updateUI();
});

BtnHolderFormTweetEL.addEventListener('click', (e) => {
	if (e.target.id === 'cancelModification') {
		// 1. CANCEL: back to mainpage
		navigateTo('mainPage');
	} else if (e.target.id === 'saveModifiedItem') {
		// 2. CHANGE: query whether AddTweet or EditTweet
		const formText = textAreaModifyTweetEl.value;
		// 3. Read <textarea>

		if (validateTweetText(formText)) {
			// 4. validateTweet, if false --> error
			const hash = location.hash;
			if (/\/add$/.test(hash)) {
				// 5.1 CHANGE in addPage
				addTweetLocal(formText);
			} else if (/\/edit\//.test(hash)) {
				// 5.2 CHANGE in editPage
				const elPosition = hash.match(/(?<=item_).+/)[0] - 1;
				changeTweetText(elPosition, formText);
			}
			// 6. back to mainPage
			navigateTo('mainPage');
		} else {
			console.log('Wrong tweet');
		}
	}
});

// ---------------------- EXPERIMENT AREA

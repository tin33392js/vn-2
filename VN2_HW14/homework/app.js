'use strict';
// ----------------------- selector
const appRoot = document.getElementById('app-root');
// --------------------- state
const stateSort = [0, 1, 2];
const arrowSort = ['↑', '↓', '↕'];
// 0: ascending
// 1: descending
// 2: none
let countrySort = 0;
let areaSort = stateSort.length - 1;
let selectSearchQueryEl, formEl, typeSearchEls, defaultOptionEl;
// --------------------- function
const readTypeSearch = () => {
	let typeSearch;
	if (document.querySelector(`input[name='search-type']:checked`)) {
		// parser of eslint not update nullish coalescing yet
		typeSearch = document.querySelector(
			`input[name='search-type']:checked`
		).value;
	}

	// Reset every time we change radio <input>
	//remove elements after default <option>
	[...selectSearchQueryEl.children].forEach((option, i) => {
		if (i > 0) {
			option.remove();
		}
	});

	if (typeSearch) {
		selectSearchQueryEl.removeAttribute('disabled');
		// use criteria to defind type-of-search
		// 'Region' / 'Language'
		const optionList = externalService[`get${typeSearch}sList`]();
		optionList.forEach((li) => {
			const optionHTML = `<option value="${li}">${li}</option>`;
			selectSearchQueryEl.insertAdjacentHTML('beforeend', optionHTML);
		});
	} else {
		//when 'checked' not exist --> undefined
		selectSearchQueryEl.setAttribute('disabled', '');
	}
};

const addInitalTable = () => {
	if (selectSearchQueryEl.hasAttribute('disabled')) {
		return;
	}
	// default state of sort
	countrySort = 0;
	areaSort = stateSort.length - 1;
	let typeSearch;
	if (document.querySelector(`input[name='search-type']:checked`)) {
		typeSearch = document.querySelector(
			`input[name='search-type']:checked`
		).value;
	}

	// reset
	if (document.querySelector('.form + *')) {
		document.querySelector('.form + *').remove();
	}

	// display based on result of <select>
	if (selectSearchQueryEl.value === '') {
		const html = `<p>No items, please choose search query</p>`;
		formEl.insertAdjacentHTML('afterend', html);
	} else {
		const initalTable = `<table class="table">
    <thead>
    <tr>
      <th class="table__country-field">Country name</th>
      <th>Capital</th>
      <th>World Region</th>
      <th>Languages</th>
      <th class="table__area-field">Area</th>
      <th>Flag</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
  </table>`;
		formEl.insertAdjacentHTML('afterend', initalTable);

		createTwoArrows(typeSearch);
		createDataTable(typeSearch);
	}
};

const createDataTable = (typeSearch) => {
	if (document.querySelector('.table tbody td')) {
		// reset dataTable
		document.querySelector('.table tbody').textContent = '';
	}
	// 1. detect type-search
	// 2. input <select> value
	let dataList = externalService[`getCountryListBy${typeSearch}`](
		selectSearchQueryEl.value
	);
	// ------------------------ sort with COUNTRY
	if (countrySort === 0) {
		// ascending [a-z]
		// 1: sort b before a
		// -1: sort a before b
		dataList.sort((a, b) => {
			return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;
		});
	} else if (countrySort === 1) {
		// descending [z-a]
		dataList.sort((a, b) => {
			return a.name.toLowerCase() < b.name.toLowerCase() ? 1 : -1;
		});
	}
	// ------------------------ sort with AREA

	if (areaSort === 0) {
		// ascending [0-9]
		dataList.sort((a, b) => {
			return a.area - b.area;
		});
	} else if (areaSort === 1) {
		// descending [9-0]
		dataList.sort((a, b) => {
			return b.area - a.area;
		});
	}

	// create table base on final 'dataList'
	dataList.forEach((countryObj) => {
		const html = `
      <tr>
      <td>${countryObj.name}</td>
      <td>${countryObj.capital}</td>
      <td>${countryObj.region}</td>
      <td>${Object.values(countryObj.languages).join(', ')}</td>
      <td>${countryObj.area}</td>
      <td><img src="${countryObj.flagURL}"></td>
      </tr>`;

		document
			.querySelector('.table > tbody')
			.insertAdjacentHTML('beforeend', html);
	});
};

const createTwoArrows = (typeSearch) => {
	// create + reset when table is created
	// add 2 arrows for 'country'  | 'area'
	document
		.querySelector('.table__country-field')
		.insertAdjacentHTML(
			'beforeend',
			`<span class="sort-btn sort-btn--country">${arrowSort[countrySort]}</span>`
		);
	document
		.querySelector('.table__area-field')
		.insertAdjacentHTML(
			'beforeend',
			`<span class="sort-btn sort-btn--area">${arrowSort[areaSort]}</span>`
		);
	// add event hanlders for 2 arrow
	const sortArrowEls = document.querySelectorAll('.sort-btn');
	sortArrowEls.forEach((el) => {
		el.addEventListener('click', () => {
			if (el.classList.contains('sort-btn--country')) {
				countrySort = countrySort === 0 ? 1 : 0;
				// set the opposite field to 'none'
				areaSort = stateSort.length - 1;
			} else {
				areaSort = areaSort === 0 ? 1 : 0;
				// set the opposite field to 'none'
				countrySort = stateSort.length - 1;
			}
			// update arrow text
			document.querySelector('.sort-btn--country').innerText =
				arrowSort[countrySort];
			document.querySelector('.sort-btn--area').innerText = arrowSort[areaSort];
			createDataTable(typeSearch);
		});
	});
};

const createFormAndH1 = () => {
	const h1HTML = '<h1 class="h1">Countries Search</h1>';
	const formHTML = ` <form class="form">
	<div>
		<p>Please choose type of search:</p>
		<div>
			<div class="search-type">
				<input type="radio" id="region" value="Region" name="search-type">
				<label for="region">By Region</label>
			</div>
			<div>
				<input type="radio" id="language" value="Language" name="search-type">
				<label for="language">By Language</label>
			</div>
		</div>
	</div>
	<div class="search-query">
		<p>Please choose search query:</p>
		<select class="search-query__select">
			<option value="" selected>Select value</option>
		</select>
	</div>

</form>`;
	appRoot.insertAdjacentHTML('beforebegin', h1HTML);
	appRoot.insertAdjacentHTML('afterend', formHTML);
};

const hotFixDeclaration = () => {
	// hot fix -.- because of I create test in html, and use direct inside function
	formEl = document.querySelector('.form');
	typeSearchEls = document.querySelectorAll(`input[name='search-type']`);
	defaultOptionEl = document.querySelector(
		'.search-query__select option[value=""]'
	);
	selectSearchQueryEl = document.querySelector('.search-query__select');
	// add event handler
	selectSearchQueryEl.addEventListener('change', addInitalTable);
	typeSearchEls.forEach((el) =>
		el.addEventListener('change', () => {
			readTypeSearch();
			addInitalTable();
		})
	);
};
// --------------------- init
createFormAndH1();
hotFixDeclaration();
readTypeSearch(); //block 2nd field

// ------------- EXPERIMENT AREA

// console.log(externalService.getCountryListByLanguage('Korean'));
// console.log(externalService.getCountryListByRegion('Asia'));
/*
write your code here

list of all regions
externalService.getRegionsList();
list of all languages
externalService.getLanguagesList();
get countries list by language
externalService.getCountryListByLanguage()
get countries list by region
externalService.getCountryListByRegion()
*/

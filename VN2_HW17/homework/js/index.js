// Your code goes here

'use strict';

const planetEl = document.querySelector('.planet');
const btnBigbang = document.querySelector('.bigbang');
const bodyEl = document.querySelector('body');

const stopSpiner = () => {
	planetEl.classList.remove('animation-spin');
};

// [task1]

const init = () => {
	//
	planetEl.classList.add('animation-spin');
	//
	fetch('https://jsonplaceholder.typicode.com/users')
		.then((response) => response.json())
		.then((data) => {
			const updateFunction = () => {
				// 1. create <ol> to hold users later added
				document
					.querySelector('script[src="js/index.js"]')
					.insertAdjacentHTML('beforebegin', '<ol class="list-user"></ol>');
				const listUserEl = document.querySelector('.list-user');
				data.forEach((user) => {
					// 2.1 add users' name as <li>
					// 2.2 with 2 btn EDIT, DELETE
					listUserEl.insertAdjacentHTML(
						'beforeend',
						`
				<li data-id="${user.id}">
					<div class="list-user--detail">
						<span class="user-name" data-id="${user.id}" >${user.name}</span>
						<button class="btn-edit">EDIT</button>
						<button class="btn-delete">DELETE</button>
					</div>
				</li>`
					);
				});
			};

			setTimeout(() => {
				stopSpiner();
				updateFunction();
			}, 1500);
		});
};

// [task2]
bodyEl.addEventListener('click', (e) => {
	// 1. read data-id from <li> parent

	if (e.target.classList.contains('btn-edit')) {
		// 2.1 take newName from prompt()
		const id = e.target.closest('li').dataset.id;
		editUserName(id, prompt('Choose your new name with 199.99$:'));
	}

	if (e.target.classList.contains('btn-delete')) {
		const id = e.target.closest('li').dataset.id;
		// 3.1
		deleteUser(id);
	}
	if (e.target.classList.contains('user-name')) {
		// 4.1
		displaySingleUserPosts(e.target.closest('li'));
	}
});

// [task3]
const editUserName = (id, newName, data) => {
	planetEl.classList.add('animation-spin');
	document
		.querySelector(`span[data-id="${id}"]`)
		.classList.add('text-animation');
	// [id,newName] from client input
	// [data] GET from server
	if (data === undefined) {
		(() => {
			// 1. GET arguement (data) used for later update
			fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
				.then((response) => response.json())
				.then((data) => editUserName(id, newName, data));
		})();
	} else {
		fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
			method: 'PUT',
			redirect: 'manual',
			// 2. copy old data, and update 'name' property
			body: JSON.stringify({
				...data,
				name: newName,
			}),
			headers: {
				'Content-type': 'application/json; charset=UTF-8',
			},
		})
			.then((response) => response.json())
			.then((data) => {
				// 3.1 select <span user-name> based on id.
				// 3.2 and change its content
				const updateFunction = () => {
					document.querySelector(`span[data-id="${id}"]`).innerText = data.name;
				};

				setTimeout(() => {
					stopSpiner();
					document
						.querySelector(`span[data-id="${id}"]`)
						.classList.remove('text-animation');
					updateFunction();
				}, 1500);
			});
	}
};

// [task4]
const deleteUser = (id) => {
	planetEl.classList.add('animation-spin');
	fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
		method: 'DELETE',
	})
		.then((response) => response.json())
		.then((data) => {
			const updateFunction = () => {
				// 1. confirm data is deleted - by check object is empty
				if (Object.keys(data).length === 0) {
					document
						.querySelector(`span[data-id="${id}"]`)
						.closest('li')
						.remove();
				}
			};
			setTimeout(() => {
				stopSpiner();
				updateFunction();
			}, 1500);
		});
};

// [task5]
const displayAllUsersPosts = () => {
	planetEl.classList.add('animation-spin');
	// 1. select parent list, instead every <li>
	const listCollection = document.querySelector('.list-user').children;
	[...listCollection].forEach((li) => {
		// reset list-post when run new
		if (document.querySelector('.list-posts')) {
			document.querySelector('.list-posts').remove();
		}
		const userId = li.dataset.id;
		// 2. take [posts]
		fetch(`https://jsonplaceholder.typicode.com/users/${userId}/posts`)
			.then((response) => response.json())
			.then((data) => {
				// 3.1 one list-posts for each users
				li.querySelector('.list-user--detail').insertAdjacentHTML(
					'afterend',
					`
				<ol class="list-posts hidden"></ol>
				`
				);
				data.forEach((post, index) => {
					// 3.2 one <li> for each post
					// insert posts below userName and btn
					li.querySelector('.list-posts').insertAdjacentHTML(
						'beforeend',
						`
						<li class="post">
							<h3>${post.title.toUpperCase()}</h3>
							<div>${post.body}</div>
						</li>
					`
					);

					fetch(
						`https://jsonplaceholder.typicode.com/posts/${post.id}/comments`
					)
						.then((response) => response.json())
						.then((data) => {
							// 4.1 each post has one list-comment
							const postEl = li.querySelector('.list-posts').children[index];
							postEl.insertAdjacentHTML(
								'beforeend',
								`
						<ul class="list-comment"></ul>
						`
							);
							data.forEach((comment) => {
								// 4.2 one <li> for each comment
								postEl.querySelector('.list-comment').insertAdjacentHTML(
									'beforeend',
									`
							<li>
								<strong>${comment.name}</strong>
								<blockquote>${comment.body}</blockquote>
							</li>
							`
								);
							});
						});
				});
			})
			.then(() => {
				// display hidden post after finish all of them
				setTimeout(() => {
					stopSpiner();
					document
						.querySelectorAll('.list-posts')
						.forEach((el) => el.classList.remove('hidden'));
					// scroll to bottom page
					window.scroll(0, 9999999);
					setTimeout(() => window.scroll(0, 0), 1500);
				}, 1500);
			});
	});
};

const displaySingleUserPosts = (el) => {
	if (el.querySelector('.list-posts')) {
		// toggle on/off
		el.querySelector('.list-posts').remove();
		return;
	}
	planetEl.classList.add('animation-spin');
	const userId = el.dataset.id;
	// 2. take [posts]
	fetch(`https://jsonplaceholder.typicode.com/users/${userId}/posts`)
		.then((response) => response.json())
		.then((data) => {
			// 3.1 one list-posts for each users
			el.querySelector('.list-user--detail').insertAdjacentHTML(
				'afterend',
				`
				<ol class="list-posts hidden"></ol>
				`
			);
			data.forEach((post, index) => {
				// 3.2 one <li> for each post
				// insert posts below userName and btn
				el.querySelector('.list-posts').insertAdjacentHTML(
					'beforeend',
					`
						<li class="post">
							<h3>${post.title.toUpperCase()}</h3>
							<div>${post.body}</div>
						</li>
					`
				);

				fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}/comments`)
					.then((response) => response.json())
					.then((data) => {
						// 4.1 each post has one list-comment
						const postEl = el.querySelector('.list-posts').children[index];
						postEl.insertAdjacentHTML(
							'beforeend',
							`
						<ul class="list-comment"></ul>
						`
						);
						data.forEach((comment) => {
							// 4.2 one <li> for each comment
							postEl.querySelector('.list-comment').insertAdjacentHTML(
								'beforeend',
								`
							<li>
								<strong>${comment.name}</strong>
								<blockquote>${comment.body}</blockquote>
							</li>
							`
							);
						});
					});
			});
		})
		.then(() => {
			// display hidden post after finish all of them
			setTimeout(() => {
				stopSpiner();
				document
					.querySelectorAll('.list-posts')
					.forEach((el) => el.classList.remove('hidden'));
			}, 1500);
		});
};

// ------------ init
init();

// ------------ add event hanlder
btnBigbang.addEventListener('click', displayAllUsersPosts);

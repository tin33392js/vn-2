# Info

(2) access: bot check, core team

![nisuuuu Gitlab](8-april.mp4)
|credit|  |
|--|--|
|music |Đếm đêm - Hacrayon, Minh Đinh  |
|CSS astronaut|Coding Artist (https://codepen.io/Coding-Artist/pen/gjZJOZ)|


___
![nisuuuu Gitlab](14-april.mp4)


|credit|  |
|--|--|
|music |SUPERSONIC Whispers by Siobhan Dakay (c) copyright    |
|wallpaper|https://wall.alphacoders.com/big.php?i=1199524|
___
(9/11) weeks \_ (19/23+3) homework

- (Week1) HW_1: create Gitlab repo, use commands
- (Week1) HW_2: create webpage with HTML only (attribute, tag)
___
- (Week2) HW_3: modify pre-build website with CSS (print, screen) - EPAM mexico
- (Week2) HW_4: using .scss to write 2 design modes - Koala compile
- (Week2) HW_5: HTML + CSS layout, flex -- ensure not broken 900px
___
- (Week3) HW_6: HTML + CSS -- responsive on mobile,tablet,desktop
- (Week3) HW_7: HTML with Boostrap -- aaaaaaaa, learning curve, look at document in every typing
- (Week3) HW_8: HTML + CSS -- try use BEM, "reuse every where"
___
- (Week4) HW_9: JS to solve logic homework -- npx eslint ./js/task1.js
- (Week4) HW_10: JS create array method by loop \_\_ (forEach, map, filter,...)
- (Week4) HW_11: JS functions -- deal with string, num, array,'rest' operator
___
- (Week5) HW_12: JS, play with localStorage, throw and try...catch error
- (Week5) HW_13: JS, work with Date. Regex is nightmare -- need to learn new vocabulary, too tired
- (Week5) HW_14: JS, DOM to country search query -- 1st craft is sh*t, lots of hotfix 
___
- (Week6) HW_15: JS, DOM build Twitter-like -- 1st time I write "code 0 đồng"
- (Week6) HW_16: JS, build OOP/class -- i feel tired and lazy around this time, don't want focus on it
- (Week6) HW_17: AJAX, fetch API, create spinner img
___
- (Week7) HW_18: HTML optimization, img, loading script
- (Week7) HW_19: JS, create functions with ES6
- (Week7) HW_20: WebPack, challenge, pain, struggle and fun. -- dependency? webpack.config? compile 1 file? bla bla. I did it but still don't know what happened
___
- (Week8) HW_21:
- (Week8) HW_22:
- (Week8) HW_23:
___
- (Week9)  Node_HW1: Build server. I try framework (Express) this time instead plain code. My server works because Postman and Internet. Can't tell why it works so good. 
- (Week10) Node_HW2:
- (Week11) Node_HW3:

## Useful commands

    cd ..

    git clone git@gitlab.com:tin33392js/vn-2.git
    git remote add origin https://gitlab.com/tin33392js/vn-2.git
    git branch -a

    git commit -am "<comment>"
    git commit -m  "<comment>" <file's path>
    git push origin main

    git branch -D <branch_name>
    git push origin --delete<branch_name>

\*when clone: not need `git init` / `git remote add`  
\* arrow key to scroll _list_  
\* key "Q" to quit _list_ interminal

### Reset

    git log
    git log <remote_name>

[1] reset: help we back on track "HEAD-mainstream" ("hard" will pull code, prefer "soft")

     git reset --soft HEAD~<number>

[2] pull won't work if we change something without commit in local (not on "HEAD-mainstream")

     git pull

[3] need turn on "Allow forced push"



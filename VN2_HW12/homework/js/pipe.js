function isFunction(functionToCheck) {
	return (
		functionToCheck && {}.toString.call(functionToCheck) === '[object Function]'
	);
}

const pipe = (value, ...funcs) => {
	// PIPE implementation
	let valueDynamic = value;

	for (let i = 0; i <= funcs.length - 1; i++) {
		try {
			if (isFunction(funcs[i])) {
				// check if it is a function
				// update 'value' from 'return' of each function
				valueDynamic = funcs[i](valueDynamic);
				console.log(`Runtime: ${i}`);
			} else {
				throw `Provided argument at position ${i} is not a function!`;
			}
		} catch (err) {
			// 'err' argument hold the 'throw exeption value'
			for (let i = 0; i <= 20; i++) {
				console.warn(err);
			}
			console.warn(err);
			console.warn(err);
			console.warn(err);
			console.warn(err);
			return err;
		}
	}
	return valueDynamic;
};

//  ------------ TEST START
// const replaceUnderscoreWithSpace = (value) => value.replace(/_/g, ' ');
// const capitalize = (value) =>
// 	value
// 		.split(' ')
// 		.map((val) => val.charAt(0).toUpperCase() + val.slice(1))
// 		.join(' ');
// const appendGreeting = (value) => `Hello, ${value}!`;

// const error = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, '');

// alert(error); // Provided argument at position 2 is not a function!

// const result = pipe(
// 	'john_doe',
// 	replaceUnderscoreWithSpace,
// 	capitalize,
// 	appendGreeting
// );

// alert(result); // Hello, John Doe!

// const error2 = pipe('john_doe', '', capitalize, '');

// alert(error2); // Provided argument at position 0 is not a function!
//  ------------ TEST END

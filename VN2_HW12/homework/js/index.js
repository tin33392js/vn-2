function visitLink(path) {
	// fakeLocal not updated <-- this js file run new
	// everytime we navigat next link == reload
	// -1 : lastindex of "Page1"
	if (localStorage.getItem(`click-page_${path.at(-1)}` !== null)) {
		localStorage.setItem(`click-page_${path.at(-1)}`, 0);
	}

	const currentClickTime = Number(
		localStorage.getItem(`click-page_${path.at(-1)}`)
	);
	// localStorage's value is 'string'
	localStorage.setItem(`click-page_${path.at(-1)}`, currentClickTime + 1);
}

function viewResults() {
	//your code goes here
	const btnEl = document.querySelector('button[onclick="viewResults()"]');
	if (document.querySelector('button[onclick="viewResults()"] + ul')) {
		// non-exist element: return 'null'
		// delete old-list each time we click
		document.querySelector('button[onclick="viewResults()"] + ul').remove();
	}

	// 1. display times we click each page

	btnEl.insertAdjacentHTML(
		// if page not click -> getItem === null -> return 0
		// without adding 0 in localStorage
		'afterend',
		`<ul>
		</li>
		<li>You visited Page3 ${
			localStorage.getItem('click-page_3') !== null
				? localStorage.getItem('click-page_3')
				: 0
		} times(s)</li>
		<li>You visited Page1 ${
			localStorage.getItem('click-page_1') !== null
				? localStorage.getItem('click-page_1')
				: 0
		} times(s)</li>
		<li>You visited Page2 ${
			localStorage.getItem('click-page_2') !== null
				? localStorage.getItem('click-page_2')
				: 0
		} times(s)
		</ul>`
	);
	// 2. clear Storage
	localStorage.clear();
}
